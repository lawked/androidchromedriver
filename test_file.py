import unittest
from time import sleep

from selenium import webdriver


class FindElementTest(unittest.TestCase):
    def setUp(self):

        capabilities = {
            'chromeOptions': {
                'androidPackage': 'com.android.chrome',
                }
        }
        self.driver = webdriver.Remote('http://localhost:9515', capabilities)

    def test_method(self):

            self.driver.get("http://apple.com")
            self.driver.find_element_by_id("gh-svg-icons").click()
            sleep(3)
            self.driver.quit()

    def tearDown(self):
        self.driver.quit()

    if __name__ == '__main__':
        unittest.main()