__author__ = 'Waldemar Dubiel'
# -*- coding: utf-8 -*-
import unittest
import sys

from test_file import FindElementTest


def suite():
    test_suite = unittest.TestSuite()

    test_suite.addTest(FindElementTest('test_method'))

    return test_suite

if __name__ == '__main__':
    result = unittest.TextTestRunner(verbosity=2).run(suite())
    sys.exit(not result.wasSuccessful())
